__author__ = 'spencer.kent'
__email__ = 'spencer.kent@berkeley.edu'

"""
This module provides some utilities for bringing spiking and movie
data from hdf5 and .mat into the python environment as well as
some preprocessing of that data to prepare for sparse poisson
analysis. To use this on your own data you will probably have
to implement your own load function
"""

from math import ceil
from itertools import cycle
from operator import itemgetter
import warnings
import numpy as np
import h5py
from scipy import io
import matplotlib.pyplot as plt


class Unit(object):
    """
    Just a simple datastructure for storing information and spiking data for a putative single unit.

    Can add fanciness to this as needed. Apologies for the verbosity of some methods due to the need
    to deal with pre-binned spike count inputs
    """
    def __init__(self, raw_timing_data=True):
        """
        Get the party started

        Args:
            raw_timing_data (Optional[bool]): Set to False in the case in which the only data
                available is pre-binned spike counts (unit won't store raw timing). Defaults to True
        """
        self.identifier = None
        self.spike_times = None
        self.isi = None
        self.binned_spike_counts = None
        self.binned_spike_count_edges = None
        self.has_raw_timing = raw_timing_data

    def SetIdentifier(self, hdf_style_str):
        """
        This method splits a string with subsequent layers of identifiers separated by '/'

        For example the default hdf group name format of 'topgroup/nextgroup/datasetname' would be
        broken up into ('topgroup', 'nextgroup', 'datasetname')

        Args:
            hdf_style_str (str): a string identifier with each subidentifier separated by '/'
        """
        if hdf_style_str[0] == '/':
            self.identifier = tuple(hdf_style_str.split('/')[1:])
        else:
            self.identifier = tuple(hdf_style_str.split('/'))

    def SetSpikeTimes(self, times_array1d):
        """
        This method stores a 1d numpy array of raw spike times (arbitrary time units)

        Args:
            times_array1d (ndarray): 1d array of spike times
        """
        if self.has_raw_timing:
            self.spike_times = np.copy(times_array1d)
        else:
            raise RuntimeError("This unit wasn't initialized to contain raw timing data")

    def SetBinnedSpikeCounts(self, counts_array1d, binedges_array1d):
        """
        This method is used to store pre-binned spike counts.

        Preference should be to store raw timing data so that arbitrary bin-sizes can be used

        Args:
            counts_array1d (ndarray): a 1d array of integer-valued spike counts
            binedges_array1d (ndarray): a 1d array with (number of bins + 1) elements denoting the
                times corresponding to the edges of each bin (arbitrary units)
        """
        if not self.has_raw_timing:
            self.binned_spike_counts = np.copy(counts_array1d)
            self.binned_spike_count_edges = np.copy(binedges_array1d)
        else:
            raise RuntimeError("This unit wasn't initialized to contain pre-binned spike counts")

    def GetIdentifier(self):
        """
        This method returns the n-tuple identifying this unit
        """
        return self.identifier

    def GetSpikeTimes(self, timeinterval=None):
        """
        This method returns the spike times stored in this unit

        If specified, timeinterval will give the bottom (closed) and top (open) interval
        from which to grab spikes

        Args:
            timeinterval (Optional[tuple]): a 2-tuple specifying the interval on which to grab
                spike times. Default is None

        Returns:
            ndarray: The 1d array of spike times for this unit
        """
        if self.has_raw_timing:
            if timeinterval != None:
                return self.spike_times[np.logical_and(self.spike_times >= timeinterval[0],
                                                       self.spike_times < timeinterval[1])]
            else:
                return self.spike_times
        else:
            raise RuntimeError("This unit wasn't initialized to contain raw timing data")

    def NumSpikes(self):
        """
        This method returns the total number of spikes recorded from this unit
        """
        if self.has_raw_timing:
            return self.spike_times.shape[0]
        else:
            return np.sum(self.binned_spike_counts)

    def InterspikeIntervals(self):
        """
        This method returns the time intervals between spikes (in arbitrary units)

        Returns:
            ndarray: A 1d array of length NumSpikes - 1 with the time interval between spikes
        """
        if self.has_raw_timing:
            if self.spike_times.shape[0] == 1:
                raise RuntimeError("Can't compute ISI on unit with only a single spike!")
            elif self.isi is None:
                self.isi = np.ediff1d(self.spike_times)
            return self.isi
        else:
            raise RuntimeError("This unit wasn't initialized to contain raw timing data")

    def BinnedSpikeCounts(self, start, stop, binwidth):
        """
        This method returns the binned spike count for bins of a specified size over a time interval

        Again, time is in arbitrary units (it's just relative to the actual raw spike times
        themselves)

        Args:
            start (float): leftmost edge of first bin
            stop (float): rightmost edge of last bin
            binwidth (float): length of time over which to count spikes for each bin

        Returns:
            ndarray: a 1d array of binned spike counts
            ndarray: a 1d array with the edges of each of the bins
        """
        # little helper function
        def find_nearest(array, value):
            idx = (np.abs(array - value)).argmin()
            return array[idx], idx

        if self.has_raw_timing:
            if self.binned_spike_counts is None or \
                    self.binned_spike_count_edges[1] - self.binned_spike_count_edges[0] != binwidth:
                binedges = [x for x in np.arange(start, stop, binwidth)] + [stop]
                self.binned_spike_counts, self.binned_spike_count_edges = np.histogram(
                    self.spike_times, binedges)
                # go ahead and grab that last spike, histogram has an open interval at top
                # but we'll define it as being closed for that last bin
                # self.binned_spike_counts[-1] += 1
            return self.binned_spike_counts, self.binned_spike_count_edges

        else:
            if self.binned_spike_count_edges[1] - self.binned_spike_count_edges[0] != binwidth:
                raise RuntimeError("\nThis unit was initialized with pre-binned spike counts. You "
                                   "can only request a binwidth of " +
                                   str(self.binned_spike_count_edges[1] -
                                       self.binned_spike_count_edges[0]))

            if start not in self.binned_spike_count_edges:
                nearest, idx_nearest = find_nearest(self.binned_spike_count_edges, start)
                start_idx = idx_nearest
                discrepancy_direction = ' later' if nearest - start > 0 else ' earlier'
                warnings.warn("\n(pre-binned unit): Requested start time not found in bin "
                                     "edges, nearest bin edge will be " +
                                     str(np.abs(nearest - start)) + discrepancy_direction +
                                     " in time than " + str(start) +" at " + str(nearest))
            else:
                start_idx = int((start - self.binned_spike_count_edges[0]) / binwidth)

            if stop not in self.binned_spike_count_edges:
                nearest, idx_nearest = find_nearest(self.binned_spike_count_edges, stop)
                stop_idx = idx_nearest
                discrepancy_direction = ' later' if nearest - stop > 0 else ' earlier'
                warnings.warn("\n(pre-binned unit): Requested stop time not found in bin "
                                     "edges, nearest bin edge will be " +
                                     str(np.abs(nearest - stop)) + discrepancy_direction +
                                     " in time than " + str(stop) +" at " + str(nearest))
            else:
                stop_idx = int((stop - self.binned_spike_count_edges[0]) / binwidth)

            return self.binned_spike_counts[start_idx:stop_idx], \
                   self.binned_spike_count_edges[start_idx:stop_idx+1]

    def NumBins(self):
        """
        Just a little convenience method to return number of bins once binned counts are computed

        Of course, with the normal raw timing units, this it totally arbitrary and depends on the
        start and end times passed to BinnedSpikeCounts())
        """
        if self.has_raw_timing:
            if self.binned_spike_counts is None:
                raise RuntimeError("You have to call the BinnedSpikeCount function first")
            else:
                return self.binned_spike_counts.size
        else:
            return self.binned_spike_counts.size

    def InterspikeIntervalsChunked(self, chunksize):
        """
        This method returns a moving average of the inter-event intervals.

        Can be used in a crude test for stationarity
        Args:
            chunksize (int): Size of the interval over which to average ISIs. The output index is
                the lower bound of this interval
        """
        if self.has_raw_timing:
            if self.spike_times.shape[0] == 1:
                raise RuntimeError("Can't compute ISI on unit with only a single spike!")
            elif self.isi is None:
                self.isi = np.ediff1d(self.spike_times)
            # The last bin or two may not be fully sized if the chunksize doesn't divide the array
            # cleanly
            return np.array([np.mean(x) for x in np.array_split(self.isi,
                                                                ceil(len(self.isi)/chunksize))])
        else:
            raise RuntimeError("This unit wasn't initialized to contain raw timing data")


def load_spiketimes_hdf_catdata(relative_hdf_filepath, subset):
    """
    This function loads the cat dataset into a collection of Unit datastructues

    It uses the functionality in h5py to walk the record, extracting spike time data according to
    the subset of spikes specified by subset parameter

    Args:
        relative_hdf_filepath (str): relative path to hdf5 record
        subset (tuple): an n-tuple specifying the subset of hdf5 groups from which to extract hdf5
            datasets. For instance ('data', 'duck8') would recursively search for all the datasets
            corresponding to stimulus 'duck8'

    Returns:
        dict[Unit]: A dictionary representing the whole dataset where each key is some unique
        identifier for a unit in a particular trial and the value is a Unit datastructure defined
        earlier in this module
    """
    units = {}
    toplevel = (''.join([directory + '/' for directory in list(subset)]))[:-1]
    def grab_h5_dataset(name, object):
        if toplevel in name:
            if isinstance(object, h5py.Dataset):
                unit = Unit()
                unit.SetIdentifier(name)
                unit.SetSpikeTimes(np.array(object))
                units[unit.GetIdentifier()[-2:]] = unit

    h5file = h5py.File(relative_hdf_filepath, 'r')
    h5file.visititems(grab_h5_dataset)
    h5file.close()

    return units


def load_spikecounts_mat_monkeydata(relative_mat_filepath, binwidth):
    """
    This takes a dataset from M. Oliver (resaved in .mat v7) and returns a set of stimuli responses

    This takes a single .mat file with cell variables 'trnresp' and 'trnstims' and organizes
    them into dictionaries of Unit structures associated to each stimulus

    Args:
        relative_mat_filepath (str): relative path to mat file
        binwidth (float): width (in seconds) of the bins used to preprocess the spike counts.
            Should be 1 / 60. for all the data he's given me so far

    Returns:
        dict[dict[Unit]]: The outer dict has keys for each stimulus and the inner dict has keys for
            each of the units recorded for that stimulus
    """

    # There's a little bit of a weird format here that deserves some
    # explaining. Mike has taken raw timing data for the spikes of sorted single units and
    # binned them into 16.7ms time bins. Before doing this, he lowpass filtered the spike
    # times so that the bins contain non-integer numbers representing spike counts (some of the
    # count gets smeared into adjacent bins). The hacky way
    # to get integer spike counts out of this is just to round each value to the nearest integer.
    # The ultimate solution is to get the raw timing data so I can bin it myself in variable sized
    # bins and not do this lowpass business (which I think is unnecessary in my case) but Mike is
    # reluctant to do any more work on this so I'm going to use what he gave me for now and if I
    # can show some results then maybe he'll compile the raw timing data for me. The integer
    # quantization will not always work, for instance
    # if 3 or more spikes got smeared by the lpf into 3 adjacent bins. It may be possible to invert
    # the transformation if I know exactly the parameters of the lpf he used but I don't have that
    # information at this time.
    #
    # The dictionary should contain two elements, 'trnresp' and 'trnstims'. 'trnstims' is
    # a 1 x s array giving the names of the s different stimuli that were used in this
    # particular recording session. Within the element
    # named 'trnresp', there's a 1 x s array  where s is the number of
    # stimuli shown in a particular recording session. For a particular stimulus, there is a 1 x n
    # array where n is the number of neurons that were simultaneously recorded from. This array is
    # itself t x p where t is the number of size binwidth time bins for the recording of that
    # particular stimulus. Mike has this weird convention where he's blocked the response into
    # 3-5s "trials"  where p is the number of trials but in reality this was one continuous
    # experiment of length t time bins (eg. 20000 * 16.7ms = 5.6minutes). The values in the columns
    # are NaN except for the 3-5s period that corresponds to that particular trial, and the
    # non-NaN values in the columns are non-overlapping. What this means is that you could either
    # just take each column's non-zero values into a 3-5s trial, or you could take the nanmean
    # across the columns to get one uber-long ~5minute trial.
    # The problem with the first method is that the "trials" as he
    # has defined them are of variable length and because of the way I'm formatting things,
    # the assumption is that all trials are of the same length. We would basically have to zero-pad
    # all the trials to the longest trial or truncate trials, neither of which is all that
    # desirable. Plus I'd prefer to have a long ~5minute trial to train on. So, off we go!

    stim_dict = {}
    matfile_dict = io.loadmat(relative_mat_filepath)
    for stim_idx in range(matfile_dict['trnresp'].shape[1]):

        stim_dict[matfile_dict['trnstims'][0, stim_idx][0]] = {}
        for unit_idx in range(matfile_dict['trnresp'][0, stim_idx].shape[1]):

            # unit name will be the stim name plus a simple numeric index because as of
            # right now I don't have any other identifying information about specific neurons
            unit_name = matfile_dict['trnstims'][0, stim_idx][0] + '/c' + str(unit_idx)
            unit = Unit(raw_timing_data=False)
            unit.SetIdentifier(unit_name)

            # In all the data I've looked at there seems to be something weird with some of the
            # last several columns in each neuron's response array. It's like there was some
            # wrap-around with the timing index or he's storing some other kind of information in
            # these last columns. That means if you take the nan-mean across columns you're going
            # to wind up with something funky in the bins in which these columns have nonzero
            # values. I've manually inspected almost all the data he gave us and this seems to be a
            # consistent problem. The most likely explanation is that there's a bug in his script
            # that generated these outputs where if the total stimulus length is longer than 20000
            # time bins (which he seems to have set for every single recording session), then the
            # counts for any subsequent time points get wrapped around back to bin index zero+.
            # Because it's not clear if this is indeed the case and I don't have the time right now
            # to track it down, I'm going to exclude from the analysis any columns that seem to
            # have this problem, which we can find programmatically by inspecting each array.

            # this gets the columns that have non-NaN values in the first row (time-bin). Any
            # column other than column zero that has this has likely suffered from the the
            # wrap-around I've guessed at (or some other problem). We'll get rid of all the columns
            # to the right of these.
            nonzero_rowone = np.where(np.isfinite(
                matfile_dict['trnresp'][0, stim_idx][0, unit_idx][0, :]))[0]

            if nonzero_rowone.size > 1:
                first_funky_column = nonzero_rowone[1]
                # often the bad column starts near the end of the 20k bins so in removing it
                # we leave some number of bins at the end of the 20k empty for the remaining
                # dataset. Let's get rid  of those rows too.
                last_row = np.where(np.isfinite(matfile_dict['trnresp'][0, stim_idx]
                                                [0, unit_idx][:, first_funky_column - 1]))[0][-1]
                # last_row + 1 is the total number of elements we would like to grab
                binedges = np.linspace(0., (last_row + 1) * binwidth, last_row + 2)

                unit.SetBinnedSpikeCounts(
                    np.rint(np.nanmean(matfile_dict['trnresp'][0, stim_idx][0, unit_idx]
                                                   [0:last_row+1, 0:first_funky_column], axis=1)),
                    binedges)
            else:
                full_shape = matfile_dict['trnresp'][0, stim_idx][0, unit_idx].shape
                binedges = np.linspace(0., full_shape[0] * binwidth, full_shape[0] + 1)
                unit.SetBinnedSpikeCounts(
                    np.rint(np.nanmean(matfile_dict['trnresp'][0, stim_idx][0, unit_idx], axis=1)),
                    binedges)

            stim_dict[matfile_dict['trnstims'][0, stim_idx][0]]['c' + str(unit_idx)] = unit

    return stim_dict


def create_spike_raster(units, colors, samplingrate, interval=None, spikewidth=2, sort_units=False):
    """
    This function takes some spike times and just plots a raster plot of spikes

    Args:
        units (dict[Unit]): a dictionary of units. The dataset for a particular trial
        colors (List[tuple[float])]): color palette for plotting
        samplingrate (float): used to get the correct time scale for the plot. For example if the
            spiketimes correspond to kHz, then sampling rate will rescale these values by 1000
        interval (Optional[tuple[float]]): time interval to plot. Default is None
        spikewidth (Optional[int]): pyplot linewidth for display. Default is 2
        sort_units (Optional[bool]): if true, then display units just based on a string sorting
            of their labels. Default false

    Returns:
        pyplot.axis: the figure axis. Can add stuff to this outside of this function.
    """
    lineheight = 1.0 / len(units)
    unit_labels = sorted(units.keys(), key=itemgetter(0, 1), reverse=True) \
        if sort_units else units.keys()
    circ_colors = cycle(colors)
    pltaxis = plt.gca()
    labels = []
    count = 0
    for u_id in unit_labels:
        plt.vlines(units[u_id].GetSpikeTimes(interval)/samplingrate, count * lineheight,
                   count * lineheight + lineheight, color=next(circ_colors), linewidth=spikewidth)
        plt.axhline(count * lineheight + lineheight, color=(208 / 255., 230 / 255., 1))
        labels.append(u_id)
        count += 1
    label_centers = np.arange(1. / (2 * len(units)), 1.0, 1. / len(units))
    plt.yticks(label_centers, labels)
    # if interval != None:
    #     plt.xticks(np.arange(interval[0], interval[1], (interval[1] - interval[0])/20),
    #                np.arange(interval[0]/samplingrate, interval[1]/samplingrate,
    #                          (interval[0] - interval[1])/(samplingrate*20)))

    plt.ylabel("Unit identifiers")
    plt.xlabel("Time (seconds)")
    return pltaxis



